angular
 .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel','ui.router'])
 .config(function($stateProvider, $urlRouterProvider) {    
  $urlRouterProvider.otherwise('/Add_Order');    
  $stateProvider       
       	  .state('Add_Order', {
            url: '/Add_Order',
           templateUrl: 'order_partial/order_add_partial.html',
           controller:'AppCtrl'
        })
   })
.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
	})
 
.controller('AppCtrl', function ($scope,$mdToast, $rootScope,$objectstore, $mdDialog, $window, $objectstore, $auth, $q, $http, $compile, $timeout) {
 


$scope.orderAccno = "";
$scope.promotionCode = "";
$scope.stoken = '62a37690791bfd8cb77e83e3b4197024';
$scope.hostUrl = '192.168.1.210';
$scope.Progressbar = false;
// get accno and promotion details start

$scope.ordertypefunc = function (oldSTBno){

    $scope.oldSTB = null;
    $scope.oldSTB = angular.copy(oldSTBno);    
    $scope.Progressbar = true;

     $http({
        method : 'GET',
        url : 'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/V5Echo/EcoService.svc/json/searchEco?searchBy=SerialNo&value='+$scope.STBno+'&withDetail=false&securityToken='+$scope.stoken+''

        }).then(function(response) {
            if(response.data.length >= 1){
                for (var i = response.data.length - 1; i >= 0; i--) {
                    $scope.orderAcc = response.data[i].PermanentRefID;
                    
                };
                $scope.getpromotionID($scope.orderAcc);

            }else{
                console.log("there is no account number available")
                $scope.orderAccno = "";
                $scope.promotionCode = "";
                $scope.Progressbar = false;
            }

           
         }, 
            function(response) { // optional
              console.log("error loading account number");
              $scope.orderAccno = "";
              $scope.Progressbar = false;
            });
 
}

$scope.getpromotionID = function(accID){
        $http({
            method : 'GET',
            url :'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/SMSAccountService/duosubscribermanagement/customer/Account.svc/Json/GetAccountInfoByGuAccountID?GUAccountID='+accID+'&SecurityToken='+$scope.stoken+''
        }).then(function(response){
             $scope.orderAccno = response.data._accountNo;
           $scope.getpromotionName(response.data._gUPromotionID);
        },function(response){
                console.log("error retrieving the promotion ID")
                $scope.Progressbar = false;
        });
}

$scope.getpromotionName = function(promoID){
    $http({
            method : 'GET',
            url :'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/SMSPromotionService/duosubscribermanagement/masters/Promotion.svc/json/RetrievePromotionByGUID?gUPromotionID='+promoID+'&securityToken='+$scope.stoken+''
        }).then(function(response){
            console.log(response.data)
            $scope.promotionCode = response.data._proPlanCode;
            $scope.Progressbar = false;

        },function(response){
                console.log("error retrieving the promotion ID")
                $scope.promotionCode = "";
                $scope.Progressbar = false;
        });
}

// get accno and promotion details end
 

// fab button start
  $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
 		$scope.save = function() {
     // setTimeout(function(){ $('#mySignup').click(); });   
     $('#mySignup').click();
        }      

//fab button end 
  
        //save functon start
        $scope.order = {};


        $scope.submit = function() {




            if ($scope.oldSTB === $scope.STBno) {

          

            }else if ($scope.oldSTB != $scope.STBno) {

                 $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        //.title('This is embarracing')
                        .content('Please Check the STB Number')
                        .ariaLabel('Invalid STB number')
                        .ok('OK')
                        .targetEvent()
                    );

            };

            
               //  var client = $objectstore.getClient("v5order");
               //  client.onComplete(function(data) {
               //      $mdDialog.show(
               //          $mdDialog.alert()
               //          .parent(angular.element(document.body))
               //          //.title('This is embarracing')
               //          .content('Activation Successfully Saved.')
               //          .ariaLabel('Alert Dialog Demo')
               //          .ok('OK')
               //          .targetEvent(data)
               //      );
               //  });

               //  client.onError(function(data) {
               //      $mdDialog.show(
               //          $mdDialog.alert()
               //          .parent(angular.element(document.body))
               //          .content('There was an error saving the data.')
               //          .ariaLabel('Alert Dialog Demo')
               //          .ok('OK')
               //          .targetEvent(data)
               //      );
               //  });

              

                               
               //  $scope.order.order_code = "-999";
               //  $scope.order.promotion = $scope.promotionCode;
               //  $scope.order.AccNo = $scope.orderAccno;
               //  $scope.order.STBno = $scope.STBno;
               // // $scope.activation.STD = self.selectedItem.display;
                
               //   client.insert($scope.order, {
               //      KeyProperty: "order_code"
               //  });


                 console.log($scope.order);

            }
            //save function end   


  
})//END OF AppCtrl
 

  