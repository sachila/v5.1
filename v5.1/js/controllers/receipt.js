angular
 .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel','ui.router'])
 .config(function($stateProvider, $urlRouterProvider) {    
  $urlRouterProvider.otherwise('/Add_Receipt');    
  $stateProvider       
          .state('Add_Receipt', {
            url: '/Add_Receipt',
           templateUrl: 'receipt_partial/receipt_add_partial.html',
           controller:'AppCtrl'
        })
   })
.config(function($mdThemingProvider) {
      $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
    })
 
.controller('AppCtrl', function ($scope,$mdToast, $rootScope,$objectstore, $mdDialog, $window, $objectstore, $auth, $q, $http, $compile, $timeout) {
    

$scope.testfun = function(){
   // $scope.orderAccno = "";
   // $scope.promotionCode = "";
}
// ng-blue function end 


$scope.orderAccno = "";
$scope.promotionCode = "";
$scope.stoken = '62a37690791bfd8cb77e83e3b4197024';
$scope.hostUrl = '192.168.1.210';
$scope.invoiceArr = {val:[]};
$scope.Progressbar = false;
// get accno and promotion details start

$scope.ordertypefunc = function (oldSTBno){

    $scope.Progressbar = true;
    $scope.oldSTB = null;
    $scope.oldSTB = angular.copy(oldSTBno);    

     $http({
        method : 'GET',
        url : 'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/V5Echo/EcoService.svc/json/searchEco?searchBy=SerialNo&value='+$scope.STBno+'&withDetail=false&securityToken='+$scope.stoken+''

        }).then(function(response) {
            if(response.data.length >= 1){
                for (var i = response.data.length - 1; i >= 0; i--) {
                    $scope.orderAcc = response.data[i].PermanentRefID;
                    
                };
                $scope.getpromotionID($scope.orderAcc);

            }else{
                console.log("there is no account number available")
                $scope.orderAccno = "";
                $scope.promotionCode = "";
                $scope.guaccID = "";
                $scope.invoiceArr = {val:[]};
                $scope.totalout="";
                $scope.Progressbar = false;
            }

           
         }, 
            function(response) { // optional
              console.log("error loading account number");
              $scope.orderAccno = "";
              $scope.Progressbar = false;
            });
 
}

$scope.getpromotionID = function(accID){
        $http({
            method : 'GET',
            url :'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/SMSAccountService/duosubscribermanagement/customer/Account.svc/Json/GetAccountInfoByGuAccountID?GUAccountID='+accID+'&SecurityToken='+$scope.stoken+''
        }).then(function(response){
          if (response.data) {
            $scope.orderAccno = response.data._accountNo;
            $scope.guaccID = response.data._gUAccountID;
            $scope.getpromotionName(response.data._gUPromotionID);
            //$scope.getinvoiceDetails(response.data._gUAccountID)
            $scope.getinvoiceDetails("15020912144506300");
            $scope.getTotaloutstanding(response.data._gUAccountID)
          }else{
              $scope.orderAccno = "";
              $scope.guaccID = "";
              $scope.invoiceArr = {val:[]};
              $scope.totalout="";
              $scope.Progressbar = false;
          }
            
          
        },function(response){
                console.log("error retrieving the Account Details ")
                $scope.Progressbar = false;
        });
         
}

$scope.getpromotionName = function(promoID){
    $http({
            method : 'GET',
            url :'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/SMSPromotionService/duosubscribermanagement/masters/Promotion.svc/json/RetrievePromotionByGUID?gUPromotionID='+promoID+'&securityToken='+$scope.stoken+''
        }).then(function(response){
            $scope.promotionCode = response.data._proPlanCode;
        },function(response){
                console.log("error retrieving the promotion ID")
                $scope.promotionCode = "";
                $scope.Progressbar = false;
        });
}

$scope.getinvoiceDetails = function(accID){
  $http({
    method : 'GET',
    url : 'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/V5Billing/DuoSubscriberBilling/InvoiceService.svc/Json/retriveInvoices?GUAccountID='+accID+'&SecurityToken='+$scope.stoken+''

  }).then(function(response){  
    $scope.invoiceArr = {val:[]};
    for (var i = response.data.length - 1; i >= 0; i--) {      
       $scope.invoiceArr.val.push({
              invoiceno : response.data[i].InvoiceAmount,
              Status : response.data[i].Status,
              date : response.data[i].InvoiceDate,
              amount : response.data[i].InvoiceAmount
            })
    };

  },function(response){
    console.log("Error Loading Invoice Data")
    $scope.Progressbar = false;

  });
}

$scope.getTotaloutstanding = function(accno){

  $http({
    url : 'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/V5Billing/DuoSubscriberBilling/ContractService.svc/json/OutstandingBalance?GUAccountID='+accno+'&SecurityToken='+$scope.stoken+''
  }).then(function(response){
    $scope.totalout = response.data.Amount;
    $scope.Progressbar = false;
  },
  function(response){
    console.log("error retrieving Total Outstanding");
    $scope.Progressbar = false;
  });

}

// get accno and promotion details end

        //save functon start
        $scope.refresh = {};

        $scope.submit = function() { 


          if ($scope.oldSTB === $scope.STBno) {

            }else if ($scope.oldSTB != $scope.STBno) {

                 $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        //.title('This is embarracing')
                        .content('Please Check the STB Number')
                        .ariaLabel('Invalid STB number')
                        .ok('OK')
                        .targetEvent()
                    );

            };  



               //  var client = $objectstore.getClient("v5refresh");
               //  client.onComplete(function(data) {
               //      $mdDialog.show(
               //          $mdDialog.alert()
               //          .parent(angular.element(document.body))
               //          //.title('This is embarracing')
               //          .content('Activation Successfully Saved.')
               //          .ariaLabel('Alert Dialog Demo')
               //          .ok('OK')
               //          .targetEvent(data)
               //      );
               //  });

               //  client.onError(function(data) {
               //      $mdDialog.show(
               //          $mdDialog.alert()
               //          .parent(angular.element(document.body))
               //          .content('There was an error saving the data.')
               //          .ariaLabel('Alert Dialog Demo')
               //          .ok('OK')
               //          .targetEvent(data)
               //      );
               //  });

              

                               
               //  $scope.refresh.refresh_code = "-999";
               // // $scope.activation.STD = self.selectedItem.display;
                
               //   client.insert($scope.refresh, {
               //      KeyProperty: "refresh_code"
               //  });
           }
            //save function end  

 
//fab button functions start

 $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
        
     

        $scope.save = function() {
            $('#mySignup').click();
        }

// fab button functio end


  
})//END OF AppCtrl
 
.filter("mydate", function() {
     return function (x) {
        return new Date(parseInt(x.substr(6)));
    };
});
  