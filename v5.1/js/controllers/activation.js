angular
 .module('mainApp', ['ngMaterial','directivelibrary','uiMicrokernel','ui.router'])
 .config(function($stateProvider, $urlRouterProvider) {    
  $urlRouterProvider.otherwise('/Add_Activation');    
  $stateProvider       
       	  .state('Add_Activation', {
            url: '/Add_Activation',
           templateUrl: 'activation_partial/activation_add_partial.html',
           controller:'AppCtrl'
        })
   })
.config(function($mdThemingProvider) {
	  $mdThemingProvider.theme('datePickerTheme').primaryPalette('teal');
	})
 
.controller('AppCtrl', function ($scope,$mdToast, $rootScope,$objectstore, $mdDialog, $window, $objectstore, $auth, $q, $http, $compile, $timeout) {

       $scope.promotionProgress = true;
       $scope.stoken = '62a37690791bfd8cb77e83e3b4197024';
       $scope.hostUrl = '192.168.1.210';
//get all promotion details start 

    $scope.promotion = [];
  
         $http({
        url: 'http://'+$scope.hostUrl+'/DuoSubscribe5/V5Services/SMSPromotionService/duosubscribermanagement/masters/Promotion.svc/json/GetAllPromoHeaderPaging?sinceID=0&securityToken='+$scope.stoken+'',
        method: "GET"
            })
            .then(function(response) {
                  console.log(response)
                  $scope.promotion = response.data;
                  $scope.promotionProgress = false;
                
            }, 
            function(response) { // optional
                    console.log("error retrieving promotion")
            });


//get all promotion details end 





 

// fab button start
  $scope.demo = {
            topDirections: ['left', 'up'],
            bottomDirections: ['down', 'right'],
            isOpen: false,
            availableModes: ['md-fling', 'md-scale'],
            selectedMode: 'md-fling',
            availableDirections: ['up', 'down', 'left', 'right'],
            selectedDirection: 'up'
        };
 		$scope.save = function() {
            $('#mySignup').click();
        }      

//fab button end 
  
        //save functon start
        $scope.activation = {};

        $scope.submit = function() {

            
                var client = $objectstore.getClient("v5actiovation");
                client.onComplete(function(data) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        //.title('This is embarracing')
                        .content('Activation Successfully Saved.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(data)
                    );
                });

                client.onError(function(data) {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .content('There was an error saving the data.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(data)
                    );
                });

              

                               
                $scope.activation.activation_code = "-999";
               // $scope.activation.STD = self.selectedItem.display;
                
                 client.insert($scope.activation, {
                    KeyProperty: "activation_code"
                });


                 console.log($scope.activation )

            }
            //save function end   


  
})//END OF AppCtrl
 

  