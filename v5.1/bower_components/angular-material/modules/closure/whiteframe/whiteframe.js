/*!
 * Angular Material Design
 * https://github.com/angular/material
 * @license MIT
 * v0.10.1-rc2-master-15e6c8c
 */
goog.provide('ng.material.components.whiteframe');

/**
 * @ngdoc module
 * @name material.components.whiteframe
 */
angular.module('material.components.whiteframe', []);

ng.material.components.whiteframe = angular.module("material.components.whiteframe");